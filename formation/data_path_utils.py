import os
from formation import utils

# lookup tables for simulations in data directory
# if run locally, it assumes data is store in project directory 
# to synch data frolder from deigo run
# rsync -avz --include="*/" --include="simulation.json" --include="evo_5000.json" --exclude="*" deigo-ext:'/bucket/FroeseU/fede/dol-simulation/*' ./data
data_path = '/bucket/FroeseU/fede/formation' if  utils.am_i_on_deigo() else 'data'

ratio_dir = os.path.join(data_path, 'ratio_min')

ratio_exp_xP_xA_xS_xN = lambda p,a,s,n: {
    'aligned': os.path.join(
        ratio_dir,
        f'{p}p_{a}a_{s}s_{n}n_zfill_noshuffle'
    ),
    'shuffled': os.path.join(
        ratio_dir,
        f'{p}p_{a}a_{s}s_{n}n_zfill'
    )
}



