from itertools import combinations
from collections import defaultdict
from sklearn.metrics.pairwise import pairwise_distances
import numpy as np
from dataclasses import dataclass
from formation.utils import modulo_radians

@dataclass
class AgentsGroup:

    agents: list

    def __post_init__(self):
        self.num_agents = len(self.agents)
        self.body_radius = self.agents[0].body_radius
        self.body_diameter = 2 * self.body_radius
        self.max_sensing_distance = self.agents[0].max_sensing_distance
        self.num_sensors = self.agents[0].num_sensors
        self.sector_angle = self.agents[0].sector_angle
        self.agent_pairs_idx = list(combinations(list(range(self.num_agents)), 2))
        self.refresh_pos_angle_distances()

    def refresh_pos_angle_distances(self):
        self.agents_pos = np.array([a.position for a in self.agents])
        self.agents_angle = np.array([a.angle for a in self.agents])
        self.compute_pairwise_distance()

    def update_pos_angle_distances(self, agents_pos, agents_angle):
        self.agents_pos = np.copy(agents_pos)
        self.agents_angle = np.copy(agents_angle)
        for i, (pos, angle) in enumerate(zip(self.agents_pos, self.agents_angle)):
            self.agents[i].position = pos
            self.agents[i].angle = angle
        self.compute_pairwise_distance()
        

    def compute_pairwise_distance(self):
        # distances between center
        self.pw_dist = pairwise_distances(self.agents_pos)
        # distances between borders
        self.pw_dist = self.pw_dist - 2 * self.body_radius

    def compute_agents_signals_strength(self, debug=False):    
        # agent_sensor_info in debug mode contains the following
        # agent_idx -> 
        #   sensor_idx -> 
        #       [ 
        #           (reached_agent_idx_1, dist_1, ang1, blocked), 
        #           (reached_agent_idx_2, dist_2, ang2, blocked), 
        #           ...
        #       ]
        agent_sensor_info = defaultdict(lambda: defaultdict(list))
        
        for i1, i2 in self.agent_pairs_idx:
            a1, a2 = self.agents[i1], self.agents[i2]
            p1, p2 = a1.position, a2.position            
            p1p2 = p2 - p1
            dist = self.pw_dist[i1,i2]

            if dist > self.max_sensing_distance:
                continue
            
            # abs perp distance between all self.agents and line passign through p1 and p2 
            # https://www.py4u.net/discuss/13656        
            abs_perp_dist = np.abs(np.cross(p1p2, self.agents_pos-p1)/np.linalg.norm(p1p2))
            
            blocked = any(
                abs_perp_dist[o] <= self.body_radius and self.pw_dist[i1,o] < dist and self.pw_dist[i2,o] < dist
                for o in range(self.num_agents) if o not in [i1,i2]
            )
            
            angle1 = modulo_radians(np.arctan2(p1p2[1],p1p2[0]))
            angle2 = modulo_radians(angle1-np.pi)
                    
            if not debug and blocked:
                # do not save info about blocked connections in non debug modes
                continue

            a1_sensor_idx = a1.get_sensor_idx(angle1)
            a2_sensor_idx = a2.get_sensor_idx(angle2)
            agent_sensor_info[i1][a1_sensor_idx].append((i2, dist, angle1, blocked))
            agent_sensor_info[i2][a2_sensor_idx].append((i1, dist, angle2, blocked))
                
        for a_idx, a_info in agent_sensor_info.items():
            sensors_dist = np.full(self.num_sensors, np.inf)
            for sens_idx, connections_info in a_info.items():
                min_dst = min(t[1] for t in connections_info) # min distance
                sensors_dist[sens_idx] = min_dst
            
            self.agents[a_idx].compute_signals_strenght(sensors_dist) 
        
        return agent_sensor_info

    def agents_forward_propagation(self):
        for a in self.agents:
            a.compute_brain_input()
            a.brain.euler_step() # compute brain_output
            a.compute_motor_outputs()        

    def move_agents(self):        
        new_agents_pos = np.copy(self.agents_pos)
        for i,a in enumerate(self.agents):
            delta_xy, delta_angle = a.plan_one_step()
            new_agents_pos[i] += delta_xy
            self.agents_angle[i] += delta_angle
        self.agents_angle = modulo_radians(self.agents_angle)
        new_pw_dist = pairwise_distances(new_agents_pos)
        collision_matrix = np.zeros((self.num_agents, self.num_agents), dtype=bool)
        for i1, i2 in self.agent_pairs_idx:
            collision_matrix[i1, i2] = collision_matrix[i2, i1] = \
                new_pw_dist[i1,i2] <= self.body_diameter
        for i,a in enumerate(self.agents):
            if not any(collision_matrix[i,:]):
                a.position = new_agents_pos[i]
            # change angle also in case of collision
            a.angle = self.agents_angle[i]

    def full_step(self, debug=False):       

        agents_pos, agents_angle = np.copy(self.agents_pos), np.copy(self.agents_angle)

        agent_sensor_info = self.compute_agents_signals_strength(debug)

        # process signal -> brain inputs -> brain outputs -> motor outpus
        self.agents_forward_propagation()
        
        # move agents one step
        self.move_agents()

        # obtain new positions and distances
        self.refresh_pos_angle_distances()

        return agents_pos, agents_angle, agent_sensor_info
