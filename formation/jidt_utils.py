import os
import jpype as jp
import numpy as np
from itertools import chain, combinations

def initJVM():
    jarLocation = os.path.join(os.getcwd(), "./", "infodynamics.jar")
    if (not(os.path.isfile(jarLocation))):
        exit("infodynamics.jar not found (expected at " + os.path.abspath(jarLocation) + ") - are you running from demos/python?")			
    jp.startJVM(jp.getDefaultJVMPath(), "-Xmx1024M", "-ea", "-Djava.class.path=" + jarLocation, convertStrings = False)   # convertStrings = False to silence the Warning while starting JVM 						

def shutdownJVM():
    jp.shutdownJVM()

def powerset(num_elements, remove_empty=False, remove_complete=False):
    """Returns all possible subset of a se with n elements

    Args:
        num_elements ([type]): [description]
        remove_empty (bool, optional): whether to remove empty set. Defaults to False.
        remove_complete (bool, optional): whether to remove complete set. Defaults to False.

    Returns:
        [type]: [description]
    """
    elements = list(range(num_elements))
    min_size = 1 if remove_empty else 0
    max_size = num_elements if remove_complete else num_elements + 1
    result = chain.from_iterable(combinations(elements, r) for r in range(min_size, max_size))
    return list(result)

def bipartitions(num_elements, sort_by_size=True):
    assert num_elements > 1

    def compute_bipartitions(collection):
        if len(collection) == 2:
            yield [[collection[0]], [collection[1]]]
            return
        first = collection[0]
        rest = collection[1:]
        # put `first` in its own subset 
        yield [[first]] + [rest]
        for rest_bp in compute_bipartitions(rest):
            # insert `first` in each of the subpartition's subsets
            for n, subset in enumerate(rest_bp):
                yield rest_bp[:n] + [[first] + subset] + rest_bp[n + 1:]


    elements = list(range(num_elements))
    bp = list(compute_bipartitions(elements))
    if sort_by_size:
        for pair in bp:
            # put smaller bipart first
            pair.sort(key=len)
    return bp    


def compute_mi(data1, data2):
    # multi variate mutual information
    jp_kraskov_pkg = jp.JPackage("infodynamics.measures.continuous.kraskov")
    mi_calc = jp_kraskov_pkg.MutualInfoCalculatorMultiVariateKraskov1()
    mi_calc.setProperty("NOISE_LEVEL_TO_ADD", "0") # no noise for reproducibility
    mi_calc.initialise(data1.shape[1], data2.shape[1]) # number of source and destination joint variables
    mi_calc.setObservations(jp.JArray(jp.JDouble, 2)(data1), jp.JArray(jp.JDouble, 2)(data2))
    result = mi_calc.computeAverageLocalOfObservations()
    return result

def compute_mi_powerset(data1, data2):
    assert data1.shape == data2.shape
    assert data1.ndim == 2
    num_rows, num_cols = data1.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    power_set_idx = powerset(num_cols, remove_empty=True)
    power_set_size = len(power_set_idx)
    mi_matrix = np.zeros((power_set_size, power_set_size))

    for i, ps_a in enumerate(power_set_idx):
        for j, ps_b in enumerate(power_set_idx):            
            sub_matrix_a = data1[:,ps_a] # column indexes specified in ps_a
            sub_matrix_b = data2[:,ps_b] # column indexes specified in ps_b
            mi_matrix[i][j] = compute_mi(sub_matrix_a, sub_matrix_b)

    mi = mi_matrix.mean()
    return mi

def compute_mi_bipartitions(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    bp = bipartitions(num_cols, sort_by_size=True)
    mi_group_size = num_cols // 2
    mi_groups_val = np.zeros(mi_group_size)
    mi_groups_len = np.zeros(mi_group_size)

    for subset_a, subset_b in bp:
        sub_data_a = data[:,subset_a] # column indexes specified in subset_a
        sub_data_b = data[:,subset_b] # column indexes specified in subset_b
        k = len(subset_a)
        mi_pair = compute_mi(sub_data_a, sub_data_b)
        mi_groups_val[k - 1] += mi_pair
        mi_groups_len[k - 1] += 1

    mi = np.sum(mi_groups_val / mi_groups_len)
    return mi

def test_powerset(num=4, remove_empty=True, remove_complete=False):        
    ps = powerset(num, remove_empty, remove_complete)
    size = len(ps)
    exp_size = 2 ** num    
    if remove_empty:
        exp_size -= 1
    if remove_complete:
        exp_size -= 1
    assert exp_size == size
    print(ps)
    print(size)

def test_bipartitions(num=4):
    bp = bipartitions(num, sort_by_size=True)
    size = len(bp)
    print('total size:', size)

    max_size_half = num // 2
    bp_dict = {s:[] for s in range(1, max_size_half+1)}
    for pair in bp:
        bp_dict[len(pair[0])].append(pair)
    bp_sized = list(bp_dict.values())

    for k, bp_size in enumerate(bp_sized,1):
        print(f'k = {k}')
        print('\n'.join(str(x) for x in bp_size))
    

def test_compute_mi():
    initJVM()
    data1 = np.random.random_sample((1000, 10))    
    # mi = compute_mi(data1, data1)    
    data2 = np.random.random_sample((1000, 10))
    # data2 = data1 + 0.5 * np.random.random_sample((1000, 10))
    mi = compute_mi(data1, data2)    
    print(mi)
    shutdownJVM()

def test_compute_mi_powerset():
    initJVM()
    data1 = np.random.random_sample((500, 4))    
    mi = compute_mi_powerset(data1, data1)    
    # data2 = np.random.random_sample((500, 4))
    # data2 = data1 + 0.5 * np.random.random_sample((500, 4))
    # mi = compute_mi_powerset(data1, data2)    
    print(mi)
    shutdownJVM()

def test_compute_mi_bipartitions():
    initJVM()
    data = np.random.random_sample((500, 4))    
    # data[:,1] = data[:,0]
    # data[:,2] = data[:,0]
    # data[:,3] = data[:,0]
    data[:,1] = data[:,0]
    data[:,3] = data[:,2]
    mi = compute_mi_bipartitions(data)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    # test_powerset(4, remove_empty=True, remove_complete=False)
    # test_bipartitions(4)
    # test_compute_mi()
    # test_compute_mi_powerset()
    test_compute_mi_bipartitions()