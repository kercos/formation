from formation.main import main

def test(p, a, s, n, noshuffle):
    params = [             
        '--dir', './data/test', 
        '--seed', '1',
        '--gen_zfill',
        '--num_pop', str(p), 
        '--pop_size', '24',                 
        '--num_agents', str(a), 
        '--num_sensors', str(s), 
        '--num_neurons', str(n), 
        '--performance_measure', 'RATIO',
        '--max_gen', '20',
        '--cores', '1'        
    ]
    if noshuffle:
        params.append('--noshuffle')
    _, evo = main(params)
    last_best_perf = evo.best_performances[-1]
    return last_best_perf

def test_synergy(p, a, s, n, noshuffle):
    params = [             
        '--dir', './data/test', 
        '--seed', '1',
        '--synergy_max',
        '--gen_zfill',
        '--num_pop', str(p), 
        '--pop_size', '4',                 
        '--num_agents', str(a), 
        '--num_sensors', str(s), 
        '--num_neurons', str(n), 
        '--max_gen', '100',
        '--cores', '4'        
    ]
    if noshuffle:
        params.append('--noshuffle')
    _, evo = main(params)
    last_best_perf = evo.best_performances[-1]
    return last_best_perf

def test_phi(p, a, s, n, noshuffle):
    params = [             
        '--dir', './data/test', 
        '--seed', '7',
        '--phi_max',
        # '--gen_zfill',
        '--num_pop', str(p), 
        '--pop_size', '48',                 
        '--num_agents', str(a), 
        '--num_sensors', str(s), 
        '--num_neurons', str(n), 
        '--max_gen', '50',
        '--cores', '4'        
    ]
    if noshuffle:
        params.append('--noshuffle')
    _, evo = main(params)
    last_best_perf = evo.best_performances[-1]
    return last_best_perf

if __name__ == "__main__":
    # test(1, 2, 4, 4, False)
    # test(4, 4, 4, 4, False)
    # test_synergy(2, 2, 4, 4, True)
    test_phi(3, 3, 4, 4, True)