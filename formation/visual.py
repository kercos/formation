"""
Implements 2D animation of experimental simulation.
"""
from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

import os
from dataclasses import dataclass
from ranges import Range, RangeSet
import tempfile
import numpy as np
import pygame
from tqdm import tqdm
import pygame.freetype  # Import the freetype module.
from formation.simulation import Simulation, test_simulation
from formation.agents_group import AgentsGroup
from formation.utils import modulo_radians, unit_vector


pygame.init()        
pygame.freetype.init() 
pygame.key.set_repeat(100)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (190, 18, 27)
BLUE = (25, 82, 184)
GREEN = (82, 240, 63)
YELLOW = (228, 213, 29)
PINK = (255, 51, 255)
ORANGE = (255, 130, 0)
TEXTFONT = pygame.font.SysFont("arial", 24)

PLOT_SENSING = 'CONNECTIONS' # 'CONNECTIONS' # 'SENSORS', 'CONNECTIONS', 'ALL', None
DEBUG_SENSING = PLOT_SENSING in ['CONNECTIONS', 'ALL']
PLOT_FORMATION = True
PLOT_CENTROID_TRACE = True # GREEN
PLOT_CENTROID_DISPLACEMENT = True # RED


GAME_FONT = pygame.freetype.Font(None, 24)

@dataclass
class Frame:

    sim: Simulation    
    canvas_size: int
    other_canvas_size: int
    zoom_factor: float    
    other_zoom_factor: float
    follow_centroid: bool
    data_record_dict: dict
    trial_idx: int
    
    def __post_init__(self):
        self.surface = pygame.Surface((self.canvas_size, self.canvas_size))

        self.is_zoom_frame = self.other_canvas_size > self.canvas_size
        
        self.agents_group = AgentsGroup(self.sim.agents)
        self.agent_idx = list(range(self.agents_group.num_agents))
        self.agent_radius = self.scale(self.sim.body_radius)
        self.formation_radius = self.scale(self.sim.formation_radius_threshold)

        self.canvas_center = np.full(2, self.canvas_size / 2)
        self.centroid_pos = self.canvas_center
        self.original_centroid_pos = self.canvas_center

        self.agents_pos_trial = self.data_record_dict['positions'][self.trial_idx]
        self.agents_ang_trial = self.data_record_dict['angles'][self.trial_idx]

        # retrieving trial data and computing centroid positions and segments
        self.original_centroid_positions = self.data_record_dict['centroid_pos'][self.trial_idx]
        self.formation = self.data_record_dict['formation'][self.trial_idx]
        travel_perf, self.valid_segments, self.centroid_segments = \
            self.sim.compute_travel_performance(self.original_centroid_positions, self.formation)
        displ_perf, self.valid_displacements, self.centroid_displacements = \
            self.sim.compute_dispacement_performance(self.original_centroid_positions, self.formation)
        self.centroid_positions = self.scale_transpose(self.original_centroid_positions, shift=False)
        self.centroid_segments = self.scale(self.centroid_segments)

        perf = \
            travel_perf if self.sim.performance_measure=='TRAVEL' \
            else displ_perf if self.sim.performance_measure=='DISPLACEMENT'\
            else 0 if travel_perf == 0 else displ_perf ** 2 / travel_perf  # RATIO

        if isinstance(self.sim, Simulation):
            # don't check perf in e.g., SimulationSynergy or SimulationEntropy
            assert perf == self.data_record_dict['trials_performances'][self.trial_idx]

    def scale_transpose(self, p, shift=True):
        if self.follow_centroid and shift:
            return self.zoom_factor *  (p - self.original_centroid_pos) + self.canvas_center
        else:
            return self.zoom_factor * p + self.canvas_center        
    
    def scale(self, p):
        return self.zoom_factor * p

    def draw_line(self, x1y1, theta, length, color, width=1):
        x2y2 = (
            int(x1y1[0] + length * np.cos(theta)),
            int(x1y1[1] + length * np.sin(theta))
        )
        pygame.draw.line(self.surface, color, x1y1, x2y2, width=width)

    def draw_agents(self, agents_pos, agents_angle):

        for ag_num, a_pos, a_angle in zip(self.agent_idx, agents_pos, agents_angle):
            # draw body
            pygame.draw.circle(self.surface, WHITE, a_pos, self.agent_radius, width=1)
            if self.is_zoom_frame:
                text = TEXTFONT.render(str(ag_num), True, WHITE)
                # text = pygame.transform.rotate(text, 180)
                text = pygame.transform.flip(text, False, True)
                self.surface.blit(text, text.get_rect(center=(a_pos[0]+30, a_pos[1]+30)))

            # draw nose
            nose_pos = a_pos + self.agent_radius * np.array([np.cos(a_angle),np.sin(a_angle)])
            self.draw_line(nose_pos, a_angle, self.agent_radius/4, PINK, width=4)

    def draw_active_sensor(self, a_pos, a_angle, sensor_idx):
        arc_rect = np.vstack([
            a_pos + np.full(2, -self.agent_radius), # start coordinate
            np.full(2, 2*self.agent_radius) # width, height
        ])
        
        start_arc = sensor_idx * self.agents_group.sector_angle + a_angle
        end_arc = (sensor_idx+1) * self.agents_group.sector_angle + a_angle
        # need negative angles for the flip reference        
        pygame.draw.arc(self.surface, BLUE, arc_rect, -end_arc, -start_arc, width=int(self.agent_radius))

    def draw_agents_sensing(self, agent_sensor_info, agents_pos, agents_angle):                        
        # agent_sensor_info in debug mode contains the following
        # agent_idx -> 
        #   sensor_idx -> 
        #       [ 
        #           (reached_agent_idx_1, dist_1, ang1, blocked), 
        #           (reached_agent_idx_2, dist_2, ang2, blocked), 
        #           ...
        #       ]
        # if debus is False no blocked connections are present
        for i, (a_pos, a_angle) in enumerate(zip(agents_pos, agents_angle)):
            poly_angles = modulo_radians(
                a_angle + \
                np.linspace(0, 2*np.pi, self.agents_group.num_sensors, endpoint=False) 
            )

            # draw slices
            for angle in poly_angles:                
                self.draw_line(a_pos, angle, self.agent_radius, RED, width=1) 

            # draw sensors
            for sensor_idx, reached_agent_info_list in agent_sensor_info[i].items():
                self.draw_active_sensor(a_pos, a_angle, sensor_idx)
                if not DEBUG_SENSING:
                    continue
                plot_connections_only = PLOT_SENSING == 'CONNECTIONS'
                non_blocking_connections = [t for t in reached_agent_info_list if not t[3]]
                if len(non_blocking_connections)==0:
                    reached_agent_idx = None
                else:
                    min_dist = min(t[1] for t in non_blocking_connections) # exclude blocking
                    reached_agent_idx = next(t[0] for t in reached_agent_info_list if t[1]==min_dist and not t[3])  # exclude blocking
                for (agent_idx, d, ang, blocked) in reached_agent_info_list:                        
                    reached_agent = agent_idx==reached_agent_idx
                    if plot_connections_only and not reached_agent:
                        continue
                    color = BLUE if reached_agent else WHITE if blocked else GREEN
                    w = 3 if reached_agent else 1
                    ray_length = self.scale(d)
                    marker_pos = a_pos + self.agent_radius * np.array([np.cos(ang), np.sin(ang)])
                    self.draw_line(marker_pos, ang, ray_length, color, width=w)
                    if plot_connections_only:
                        pygame.draw.circle(self.surface, color, marker_pos, self.agent_radius/8)            
    
    def draw_formation(self, centroid_positions, valid_segments, centroid_segments,
                       valid_displacements, centroid_displacements):  

        centroid_pos_shifted = self.scale_transpose(self.original_centroid_pos)

        centroid_color = YELLOW if self.currently_in_formation else WHITE
        w = 3 if self.currently_in_formation else 1
        pygame.draw.circle(self.surface, centroid_color, centroid_pos_shifted, self.formation_radius, width=w)
        pygame.draw.circle(self.surface, GREEN, centroid_pos_shifted, 3)
        
        # TRAVEL
        if PLOT_CENTROID_TRACE:
            for valid_segment, centroid_seg, centroid_pos in zip(
                    valid_segments, centroid_segments, centroid_positions):
                                
                trace_color = GREEN if valid_segment else WHITE
                w = 3 if valid_segment else 1
                start_segment = centroid_pos
                end_segment = centroid_pos + centroid_seg
                pygame.draw.line(self.surface, trace_color, start_segment, end_segment, width=w)        
        
        # DISPACEMENT
        if PLOT_CENTROID_DISPLACEMENT:
            valid_centroid_displacements = \
                centroid_displacements[valid_displacements]
            
            range_set = RangeSet(
                Range(*p) 
                for p in valid_centroid_displacements
            )
            uv = unit_vector(self.centroid_pos - self.canvas_center)
            for r in range_set:                        
                start_segment = self.scale_transpose(r.start * uv)
                end_segment = self.scale_transpose(r.end * uv)
                pygame.draw.line(self.surface, RED, start_segment, end_segment, width=2) 

    def final_tranform_main_surface(self):
        '''
        final transformations:
        - shift coordinates to conventional x=0, y=0 in bottom left corner
        - zoom...
        '''
        self.surface.blit(pygame.transform.flip(self.surface, False, True), dest=(0, 0))


    def run_step(self, i):
        # update values at current time steps
        self.original_centroid_pos = self.original_centroid_positions[i]
        self.centroid_pos = self.centroid_positions[i]    
        self.currently_in_formation = self.formation[i]

        agents_pos = self.agents_pos_trial[i]
        agents_angle = self.agents_ang_trial[i]            

        self.agents_group.update_pos_angle_distances(agents_pos, agents_angle)

        _, _, agent_sensor_info = self.agents_group.full_step(DEBUG_SENSING)
        
        agents_pos = self.scale_transpose(agents_pos)    

        # reset canvas
        self.surface.fill(BLACK)
            
        # draw agents
        self.draw_agents(agents_pos, agents_angle)       

        # draw self.formation
        if PLOT_FORMATION:
            self.draw_formation(
                self.scale_transpose(self.original_centroid_positions[:i]), 
                self.valid_segments[:i], 
                self.centroid_segments[:i],
                self.valid_displacements[:i], 
                self.centroid_displacements[:i]
            )

        # draw agents sensors
        if PLOT_SENSING is not None:     
            self.draw_agents_sensing(agent_sensor_info, agents_pos, agents_angle)

        # draw frame
        if self.other_zoom_factor is not None:
            if self.follow_centroid:
                pygame.draw.rect(self.surface, ORANGE, ((0,0),(self.canvas_size, self.canvas_size)), width = 4)
            else:
                size_zoom_window_in_main = self.other_canvas_size / self.other_zoom_factor * self.zoom_factor
                left_bottom = self.centroid_pos - [size_zoom_window_in_main/2] * 2
                width_height = [size_zoom_window_in_main] * 2
                pygame.draw.rect(self.surface, ORANGE, (left_bottom, width_height), width = 1)

        # final traformations
        self.final_tranform_main_surface()


@dataclass
class Visualization:

    sim: Simulation
    canvas_size: int = 800
    main_zoom_factor: float = None # auto
    focus_zoom_factor: float = None # auto
    zoom_canvas_size: int = 350
    fps: float = 30
    video_path: str = None

    def __post_init__(self):    
        self.video_mode = self.video_path is not None        
                
        if self.video_mode:
            self.video_tmp_dir = tempfile.mkdtemp(dir=os.path.dirname(self.video_path))
            self.surface = pygame.Surface((self.canvas_size, self.canvas_size))
        else:
            self.surface = pygame.display.set_mode((self.canvas_size, self.canvas_size))         

    def export_video(self):
        import ffmpeg
        import shutil
        (
            ffmpeg
            .input(f'{self.video_tmp_dir}/*.png', pattern_type='glob', framerate=self.fps)
            .output(self.video_path, pix_fmt='yuv420p')
            .overwrite_output()
            .run(quiet=True)
        )        
        shutil.rmtree(self.video_tmp_dir)

    def start(self, data_record_dict, trial_idx):

        centroid_positions = data_record_dict['centroid_pos'][trial_idx]

        if self.focus_zoom_factor is None:
            # detect focus zoom factor automatically based on formation_radius_threshold
            self.focus_zoom_factor = \
                self.zoom_canvas_size / self.sim.formation_diameter_threshold

        if self.main_zoom_factor is None:
            # detect main zoom factor automatically based on furthest reached point

            # distance between centroid and its starting position (origin) at each step
            centr_dist_from_start = np.hypot( 
                centroid_positions[:,0], 
                centroid_positions[:,1]
            )

            centroid_max_dist_origin = np.max(centr_dist_from_start)

            self.main_zoom_factor = \
                self.canvas_size / ( 
                    2 * (centroid_max_dist_origin + 
                    self.sim.formation_radius_threshold)
                )
            
            if self.main_zoom_factor >= self.focus_zoom_factor:
                self.focus_zoom_factor = None # do not use zoom frame

        use_zoom_frame = self.focus_zoom_factor is not None

        main_frame = Frame(
            sim=self.sim,
            canvas_size=self.canvas_size,
            other_canvas_size=self.zoom_canvas_size,
            zoom_factor=self.main_zoom_factor,            
            other_zoom_factor=self.focus_zoom_factor,
            follow_centroid=False,
            data_record_dict=data_record_dict,
            trial_idx=trial_idx
        )
        
        if use_zoom_frame:
            zoom_frame = Frame(
                sim=self.sim,
                canvas_size=self.zoom_canvas_size,
                other_canvas_size=self.canvas_size,
                zoom_factor=self.focus_zoom_factor,
                other_zoom_factor=self.main_zoom_factor,            
                follow_centroid=True,
                data_record_dict=data_record_dict,
                trial_idx=trial_idx
            )

        centroid_final_pos = centroid_positions[-1]
        cp = centroid_final_pos > 0 
        # True, True meaning it means it is in the top right quadrant     

        if use_zoom_frame:
            x_dest_zoom_frame = 0 if cp[0] else self.canvas_size - zoom_frame.canvas_size
            y_dest_zoom_frame = self.canvas_size - zoom_frame.canvas_size if cp[1] else 0

        step_text_pos = (self.canvas_size-120, 10) if cp[0] else (10, 10)

        num_zeros = int(np.ceil(np.log10(self.sim.num_steps)))

        running = True
        pause = False

        clock = pygame.time.Clock()        

        pbar = tqdm(total=self.sim.num_steps)

        i = 0

        while running and i < self.sim.num_steps:

            if not self.video_mode:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        running = False
                    elif event.type == pygame.KEYDOWN:
                        ekey = event.key
                        if ekey == pygame.K_ESCAPE:
                            running = False
                        elif ekey == pygame.K_p:
                            pause = not pause
                        elif pause:
                            if ekey == pygame.K_LEFT and i>0:                                
                                i -= 1
                            elif ekey == pygame.K_RIGHT and i < self.sim.num_steps-1:
                                i += 1

                

            self.surface.fill(BLACK)
            main_frame.run_step(i)
            if use_zoom_frame:
                zoom_frame.run_step(i)

            self.surface.blit(main_frame.surface, dest=(0, 0))
            
            if use_zoom_frame:
                self.surface.blit(zoom_frame.surface, dest=(x_dest_zoom_frame, y_dest_zoom_frame))
            
            step = str(i+1).zfill(num_zeros)
            
            GAME_FONT.render_to(self.surface, step_text_pos, f"Step: {step}", WHITE)            

            if self.video_mode:
                filepath = os.path.join(self.video_tmp_dir, f"{step}.png")
                pygame.image.save(self.surface, filepath)
                pbar.update()
            else:                
                pygame.display.update()
                clock.tick(self.fps)

            if not pause:
                i += 1            

        if self.video_mode:
            self.export_video()
        
        pygame.quit()


def test_visual_sim():
    sim, data_record_dict = test_simulation(
        num_agents=2,
        num_sensors=8,
        num_neurons=10,
        num_steps=500,        
        seed=259005137,        
    )
    viz = Visualization(
        sim,
        # video_path='video/test.mp4'
    )
    viz.start(data_record_dict, trial_idx=0)

if __name__ == "__main__":
    test_visual_sim()
