"""
Runs evolutionary code for the main simulation.
Run from command line as
python -m formation.main args
See help for required arguments:
python -m formation.main --help
"""

from formation.simulation_synergy import SimulationSynergy
from formation.simulation_entropy import SimulationEntropy
from formation.simulation_mediano_phi import SimulationMedianoPhi
import os
import argparse
from pytictoc import TicToc
import numpy as np
from pyevolver.evolution import Evolution
from formation import gen_structure
from formation import utils
from formation.simulation import Simulation
from formation import jvm


def main(raw_args=None):
    parser = argparse.ArgumentParser(
        description='Run the Formation Simulation'
    )

    # evolution arguments
    parser.add_argument('--seed', type=int, default=0, help='Random seed')
    parser.add_argument('--dir', type=str, default=None, help='Output directory')
    parser.add_argument('--gen_zfill', action='store_true', default=False,
                        help='whether to fill geotipes with zeros otherwize random (default)')    
    parser.add_argument('--num_pop', type=int, default=1, help='Number of populations')
    parser.add_argument('--pop_size', type=int, default=96, help='Population size')
    parser.add_argument('--noshuffle', action='store_true', default=False, help='Weather to shuffle agents before eval function')
    parser.add_argument('--max_gen', type=int, default=10, help='Number of generations')

    # simulation arguments        
    parser.add_argument('--num_agents', type=int, default=1, help='Number of agents in simulation')
    parser.add_argument('--num_sensors', type=int, default=4, help='Number of sensors in each agent')
    parser.add_argument('--num_neurons', type=int, default=2, help='Number of brain neurons in each agent')
    parser.add_argument('--num_steps', type=int, default=500, help='Number of simulation steps')    
    parser.add_argument('--performance_measure', type=str, default='RATIO', \
        choices=["TRAVEL", "DISPLACEMENT", "RATIO"], help='Type of perf measure')
    parser.add_argument('--synergy_max', action='store_true', default=False, help='Weather to use synergy maximization')
    parser.add_argument('--entropy_max', action='store_true', default=False, help='Weather to use entropy maximization')
    parser.add_argument('--phi_max', action='store_true', default=False, help='Weather to use entropy maximization')
    parser.add_argument('--cores', type=int, default=1, help='Number of cores')

    # Gather the provided arguements as an array.
    args = parser.parse_args(raw_args)

    t = TicToc()
    t.tic()

    if args.dir is not None:
        # create default path if it specified dir already exists
        if os.path.isdir(args.dir):
            subdir = 'synergy_' if args.synergy_max \
                else 'entropy_' if args.entropy_max \
                else 'phi_' if args.phi_max \
                else ''
            subdir += f'{args.num_pop}p_{args.num_agents}a_{args.num_sensors}s_{args.num_neurons}n'
            if args.gen_zfill:
                subdir += '_zfill'
            if args.noshuffle:
                subdir += f'_noshuffle'
            seed_dir = 'seed_{}'.format(str(args.seed).zfill(3))
            outdir = os.path.join(args.dir, subdir, seed_dir)
        else:
            # use the specified dir if it doesn't exist 
            outdir = args.dir
        utils.make_dir_if_not_exists_or_replace(outdir)
    else:
        outdir = None

    checkpoint_interval = 50 # int(np.ceil(args.max_gen / 100))

    sim_class = SimulationSynergy if args.synergy_max \
        else SimulationEntropy if args.entropy_max \
        else SimulationMedianoPhi if args.phi_max \
        else Simulation

    sim = sim_class(
        num_agents=args.num_agents,
        num_sensors = args.num_sensors,
        num_neurons = args.num_neurons,
        num_steps = args.num_steps,
        performance_measure = args.performance_measure,
        num_cores=args.cores
    )

    genotype_size = sim.genotype_size

    if outdir is not None:
        sim_config_json = os.path.join(outdir, 'simulation.json')
        sim.save_to_file(sim_config_json)

    population = None  # by default randomly initialized in evolution

    if args.gen_zfill:
        # all genotypes initialized with zeros
        population = np.zeros(
            (args.num_pop, args.pop_size, genotype_size)
        )

    evo = Evolution(
        random_seed=args.seed,
        population=population,
        num_populations=args.num_pop,
        shuffle_agents=not args.noshuffle,
        population_size=args.pop_size,
        genotype_size=genotype_size,
        evaluation_function=sim.evaluate,
        performance_objective='MAX',
        fitness_normalization_mode='FPS',  # 'NONE', 'FPS', 'RANK', 'SIGMA' -> NO NORMALIZATION
        selection_mode='RWS',  # 'UNIFORM', 'RWS', 'SUS'
        reproduce_from_elite=False,
        reproduction_mode='GENETIC_ALGORITHM',  # 'HILL_CLIMBING',  'GENETIC_ALGORITHM'
        mutation_variance=0.05,  # mutation noice with variance 0.1
        elitist_fraction=0.05,  # elite fraction of the top 4% solutions
        mating_fraction=0.95,  # the remaining mating fraction (consider leaving something for random fill)
        crossover_probability=0.1,
        crossover_mode='UNIFORM',
        crossover_points=None,  # genotype_structure['crossover_points'],
        folder_path=outdir,
        max_generation=args.max_gen,
        termination_function=None,
        checkpoint_interval=checkpoint_interval
    )
    print('Output path: ', outdir)
    print('n_elite, n_mating, n_filling: ', evo.n_elite, evo.n_mating, evo.n_fillup)
    evo.run()

    print('Ellapsed time: {}'.format(t.tocvalue()))

    return sim, evo


if __name__ == "__main__":
    main()
