"""
Available data:
angles: heading directions
brain_derivatives, _inputs, _outputs, _states
centroid_pos: x,y-coordinates of the center of mass of formation
formation: if the group is in formation
genotype_distance: genotype similarity of the agents
genotype_index: the index of the pair that is selected (the first one by default)
genotypes: genotypes of the stored agents
motors: state of the motors
performance: final performance of the current pair
phenotypes: network parameters (built from genotype)
positions: x,y-coordinates of all agents
sensors: state of the sensors
signals: input to the sensors
signatures: agent IDs
trials_performances: performance for every trial
"""

import json
import pandas as pd
import numpy as np
import csv

data_dir = 'data/team3seed2/'
center_pos_file = '{}centroid_pos.json'.format(data_dir)
brain_outputs_file = '{}brain_outputs.json'.format(data_dir)
positions_file = '{}positions.json'.format(data_dir)

with open(center_pos_file) as f:
    center_pos = json.load(f)
with open(brain_outputs_file) as f:
    brain_output = json.load(f)
with open(positions_file) as f:
    positions = json.load(f)

# center position
center_vec = np.array(center_pos)
num_trials, num_steps, num_dims = center_vec.shape
center_col_names = ['trial', 'timestep', 'x', 'y']

with open('center_pos_out.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(center_col_names)
    for i in range(num_trials):
        for j in range(num_steps):
            writer.writerow([i, j, center_vec[i][j][0], center_vec[i][j][1]])


# agent positions
pos_vec = np.array(positions)
num_trials, num_steps, num_agents, num_dims = pos_vec.shape
pos_col_names = ['trial', 'timestep', 'agent', 'x', 'y']

with open('pos_out.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(pos_col_names)
    for i in range(num_trials):
        for j in range(num_steps):
            for k in range(num_agents):
                writer.writerow([i, j, k, pos_vec[i][j][k][0], pos_vec[i][j][k][1]])


# center position
brain_vec = np.array(brain_output)
num_trials, num_steps, num_agents, num_neurons = brain_vec.shape
brain_col_names = ['trial', 'timestep', 'agent', 'n1', 'n2', 'n3', 'n4']

with open('brain_out.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(brain_col_names)
    for i in range(num_trials):
        for j in range(num_steps):
            for k in range(num_agents):
                writer.writerow([i, j, k,
                                 brain_vec[i][j][k][0],
                                 brain_vec[i][j][k][1],
                                 brain_vec[i][j][k][2],
                                 brain_vec[i][j][k][3]],
                                )
