# Add running windows (span 20 units - overlapping)
# Only trial 0 (first trial)
# Only one agent (agent 0)
# Xn = (500,4) - brain output of agent 0
# Xd = (500,3) - distance of agents from origin
# Va = (500,1) - distance of agent from origin
# Vc = (500,1) - distance of center of mass from origin

# C2I = Psi(Xd,Vc) - 4 numbers: 1 per trial
# I2N = Psi(Xn,Va) - 12 numbers:  1 per trial, 1 per agent
# C2N = Psi(Xn,Vc) - 12 numberss: 1 per trial, 1 per agent
# Analysis

from formation.run_from_dir import run_simulation_from_dir
import numpy as np
from tqdm import tqdm
import csv
from measures.mediano.mediano_psi import MedianoPsi
from measures.mediano.mediano_delta import MedianoDelta
from measures.mediano.mediano_gamma import MedianoGamma
import matplotlib.pyplot as plt
import os

# DATA_DIR = 'data/ratio_mean/3p_3a_4s_4n_zfill_noshuffle'
DATA_DIR = 'data/displacement_mean/3p_3a_4s_4n_zfill_noshuffle'

def emergence_psi_window(x, v, win_length=20):
    data_len = len(x)
    assert data_len % win_length == 0
    num_parts = data_len - win_length
    result = np.zeros(num_parts)
    bar = tqdm(total=num_parts)
    for s in range(num_parts):
        bar.update()        
        e = s + win_length  # end
        r = MedianoPsi(x[s:e], v[s:e])
        result[s] = r
    return result


def get_phi_measure(data_record_dict, tau, measure_function, use_distance, differenciate):
    brain_out = data_record_dict['brain_outputs']   # (4, 500, 3, 4) --- (trials, steps, agents, neurons)
    agents_pos = data_record_dict['positions']       # (4, 500, 3, 2) --- (trials, steps, agents, xy)
    centroid_pos = data_record_dict['centroid_pos']    # (4, 500, 2) --- (trials, steps, xy)

    num_trials, num_steps, num_agents, num_neurons = brain_out.shape

    c2i = np.zeros(num_trials)
    i2n = np.zeros((num_trials, num_agents))
    c2n = np.zeros((num_trials, num_agents))

    for trial_idx in range(num_trials):

        centroid_pos_t = centroid_pos[trial_idx]  # (500,2)        
        pos_t_agents = agents_pos[trial_idx]  # (500,3,2)

        if use_distance:
            centroid_dst_t = np.linalg.norm(centroid_pos_t, axis=1)  # distance from origin - 500 values            
            centroid_dst_t = np.expand_dims(centroid_dst_t, axis=1)                        
            dst_t_agents = np.linalg.norm(pos_t_agents, axis=2)  # (500,3) distance from origin of all agents
            centroid_values = centroid_dst_t 
            agents_values = dst_t_agents
        else: 
            centroid_values = centroid_pos_t # (500,2)
            agents_values = pos_t_agents # (500,3,2)

        if differenciate:
            centroid_values = np.diff(centroid_values, axis=0)    
            agents_values = np.diff(agents_values, axis=0)
        c2i[trial_idx] = measure_function(agents_values, centroid_values, tau)
        
        for agent_idx in range(num_agents):
            brain_t_a = brain_out[trial_idx, :, agent_idx]  # (500, 4) neurons of selected agents
            if differenciate:
                brain_t_a = brain_t_a[:-1] # same number of rows (499)
            a_values = agents_values[:, agent_idx]  
            if a_values.ndim==1:
                a_values = np.expand_dims(a_values, axis=1)
            i2n[trial_idx, agent_idx] = measure_function(brain_t_a, a_values, tau)
            c2n[trial_idx, agent_idx] = measure_function(brain_t_a, centroid_values, tau)
            '''
            i2n_window = emergence_psi_window(brain_t_a, dst_t_a)
            print(i2n_window.mean())
            plt.plot(i2n_window)
            plt.show()
            '''

    return c2i, i2n, c2n


def get_seeds_measure(measure_name, measure_function, gen, tau, use_distance, differenciate, write_csv=False, plot=False):
    if write_csv:
        fout = open(f'processed_data/3a_{measure_name}_seeds.csv', 'w')
        csv_writer = csv.writer(fout)
        csv_writer.writerow(['seed', f'{measure_name}1', f'{measure_name}2',
                            f'{measure_name}3', 'performance'])

    num_seeds = 20
    seed_results = {}

    for s in range(num_seeds): 
        seed = s + 1
        seed_zfill = str(seed).zfill(3)

        data_dir = os.path.join(DATA_DIR, f'seed_{seed_zfill}') 
        performance, trials_performances, evo, sim, data_record_dict = run_simulation_from_dir(data_dir, gen)

        # m1 = one value per trial; m2, m3 = rows are different trials, cols are agents
        m1, m2, m3 = get_phi_measure(data_record_dict, tau, measure_function, use_distance, differenciate)
        m1_mean, m2_mean, m3_mean = m1.mean(), m2.mean(), m3.mean()

        seed_results[seed] = {
            'm1_mean': m1_mean,
            'm2_mean': m2_mean,
            'm3_mean': m3_mean,
            # 'perf': performance,
        }        

        if write_csv:
            csv_writer.writerow([seed_zfill, m1_mean, m2_mean, m3_mean, round(performance, 2)])

        print('seed', seed_zfill)
        print(f'{measure_name}1 mean', m1_mean)
        print(f'{measure_name}2 mean', m2_mean)
        print(f'{measure_name}3 mean', m3_mean)

    if write_csv:
        fout.close()

    if plot:
        fig, ax = plt.subplots(layout='constrained')
        x = np.arange(num_seeds)
        width = 0.25  # the width of the bars
        seeds = list(seed_results.keys())
        measures = seed_results[seeds[0]].keys()
        for i,m in enumerate(measures):
            offset = width * i
            m_values = [round(seed_results[s][m],1) for s in seeds]
            rects = ax.bar(x + offset, m_values, width, label=m)
            ax.bar_label(rects, padding=3)

        # ax.set_yscale("log")
        ax.set_xticks(x + width, seeds)
        ax.legend()
        # plt.show()
        fig.savefig(f'plots/gen_{gen}_{measure_name}_tau_{tau}.png')
        

    return seed_results



def get_generations_measure(seed_num, measure_name, measure_function, use_distance, differenciate, log=False):

    seed_zfill = str(seed_num).zfill(3)

    output_dir = os.path.join('analysis', measure_name)
    os.makedirs(output_dir, exist_ok=True)

    output_file = os.path.join(output_dir, f'3a_{measure_name}_seed_{seed_zfill}_generations.csv')

    fout = open(output_file, 'w')
    csv_writer = csv.writer(fout)
    csv_writer.writerow(['seed', f'{measure_name}1', f'{measure_name}2',
                         f'{measure_name}3', 'performance'])

    data_dir = os.path.join(DATA_DIR, f'seed_{seed_zfill}') 
    # [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
    # generations = list(range(0, 500, 50)) + list(range(500, 5001, 500))
    generations = list(range(0, 5001, 50))

    for gen in generations:
        performance, trials_performances, evo, sim, data_record_dict = run_simulation_from_dir(data_dir, gen=gen)
        m1, m2, m3 = get_phi_measure(data_record_dict, 1, measure_function, use_distance, differenciate)
        m1_mean, m2_mean, m3_mean = m1.mean(), m2.mean(), m3.mean()
        csv_writer.writerow([gen, m1_mean, m2_mean, m3_mean, round(performance, 2)])

        if log:
            print(f'seed {seed_num} gen {gen}')
            print(f'{measure_name}1 mean', m1_mean)
            print(f'{measure_name}2 mean', m2_mean)
            print(f'{measure_name}3 mean', m3_mean)

    fout.close()


def get_measure_seed_gen(seed_num, gen_num, measure_function, use_distance, differenciate):
    seed_zfill = str(seed_num).zfill(3)
    data_dir = os.path.join(DATA_DIR, f'seed_{seed_zfill}')

    performance, trials_performances, evo, sim, data_record_dict = run_simulation_from_dir(data_dir, gen=gen_num)
    m1, m2, m3 = get_phi_measure(data_record_dict, 1, measure_function, use_distance, differenciate)

    print(f'seed {seed_num} gen {gen_num}')
    print('m1')
    for trial_num in range(m2.shape[0]):
        print(f'trial {trial_num+1}', m1[trial_num])
    print(f'mean: {np.mean(m1)}')
    print()

    print('m2')
    for trial_num in range(m2.shape[0]):
        print(f'trial {trial_num+1}', m2[trial_num])
    print(f'mean: {np.mean(m2)}')
    print()

    print('m3')
    for trial_num in range(m3.shape[0]):
        print(f'trial {trial_num+1}', m3[trial_num])
    print(f'mean: {np.mean(m3)}')

def analyze_all_seeds(use_distance, differenciate):
    for tau in [1,2,5,10,20]:
        get_seeds_measure('psi', MedianoPsi, gen=50, tau=tau, use_distance=use_distance, differenciate=differenciate, plot=True)
        get_seeds_measure('delta', MedianoDelta, gen=50, tau=tau, use_distance=use_distance, differenciate=differenciate, plot=True)
        get_seeds_measure('gamma', MedianoGamma, gen=50, tau=tau, use_distance=use_distance, differenciate=differenciate, plot=True)

def analyze_all_generations(measure='psi', measure_func=MedianoPsi, use_distance=True, differenciate=False, num_cores=1):
    from joblib import Parallel, delayed
    seeds = list(range(1, 21))
    if num_cores==1:
        for s in seeds:
            print("Analyzing seed: ", s)
            get_generations_measure(s, measure, measure_func, use_distance, differenciate, log=True)
    else:
        Parallel(n_jobs=num_cores)(
            delayed(get_generations_measure)(s, measure, measure_func, use_distance, differenciate, log=False) \
            for s in seeds
        )



if __name__ == "__main__":
    # analyze_all_seeds()
    analyze_all_generations(measure='psi', measure_func=MedianoPsi, num_cores=1)
    # analyze_all_generations(measure='delta', measure_func=MedianoDelta, num_cores=4)
    # analyze_all_generations(measure='gamma', measure_func=MedianoGamma, num_cores=4)

    # intermediate values for specific seed and generation
    # get_measure_seed_gen(17, 5000, MedianoPsi, use_distance=True, differenciate=True)
