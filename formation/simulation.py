"""
Main code for experiment simulation.
"""

from dataclasses import dataclass, asdict
from formation.agents_group import AgentsGroup
import json
import numpy as np
from numpy.random import RandomState
from joblib import Parallel, delayed
from ranges import Range, RangeSet
from pyevolver.json_numpy import NumpyListJsonEncoder
from formation.agent import Agent
from formation import gen_structure
from formation import utils
from scipy.spatial import distance

# TODO: needs to decide which one to use 
# or incorporate this param in simulation
PERF_FUNC = 'mean' # min, mean

@dataclass
class Simulation:

    num_agents: int = 2 # number of agents (>1)    
    num_sensors: int = 4 # number of sensors (>1)
    num_neurons: int = 2 # number of brain neurons
    brain_step_size: float = 0.1
    body_radius: float = 10.

    # radius of the circle oround (0,0) where agents are initially placed
    initial_distribution_radius: float = 50.

    # dist between boundaries of each agent pairs
    max_sensing_distance: float = 200. 

    # diameter of the circle centered on threshold to act as a boundary for formation condition
    # agents are in formation if their centers is inside or on the boundary of tis circle (yellow in visualization)
    # make it max_sensing_distanceif formation conditions prefectly coincides with sensing
    formation_diameter_threshold: float = 200.

    # performance measure
    # TRAVEL: sum of lengths of valid segment movements of centroid
    # DISPLACEMENT: sum of dispacement of centroid away from its initial position
    performance_measure: str = "RATIO" # "TRAVEL", "DISPLACEMENT", "RATIO"

    num_trials: int = 4
    num_steps: int = 500
    num_cores: int = 1    

    def __post_init__(self):

        self.__check_params__()   

        self.num_motors = 2 # always 2 motors

        self.genotype_structure = gen_structure.DEFAULT_GEN_STRUCTURE(
            self.num_sensors, self.num_neurons, self.num_motors)

        self.genotype_size = gen_structure.get_genotype_size(self.genotype_structure)

        self.formation_radius_threshold = self.formation_diameter_threshold/2
        
        self.agents = [
            Agent(
                body_radius=self.body_radius,
                max_sensing_distance=self.max_sensing_distance,
                genotype_structure=self.genotype_structure,
                brain_step_size=self.brain_step_size,
            )
            for _ in range(self.num_agents)
        ]      

        # init centroid (agents center of mass) for computing performance
        self.centroid_pos = np.zeros((self.num_trials, self.num_steps, 2))
        self.centroid_segments = np.zeros((self.num_trials, self.num_steps-1, 2))
                
        # if agents are in formation (close together to centroid)
        self.formation = np.zeros((self.num_trials, self.num_steps), dtype=bool)



    def __check_params__(self):
        utils.assert_string_in_values(
            self.performance_measure, 
            'performance_measure',
            ["TRAVEL", "DISPLACEMENT", "RATIO"]
        )        


    def save_to_file(self, file_path):
        with open(file_path, 'w') as f_out:
            obj_dict = asdict(self)
            json.dump(obj_dict, f_out, indent=3, cls=NumpyListJsonEncoder)

    @staticmethod
    def load_from_file(file_path, **kwargs):
        with open(file_path) as f_in:
            obj_dict = json.load(f_in)

        if kwargs:
            for k,v in kwargs.items():
                if v is None or k not in obj_dict:
                    continue                
                old_v = obj_dict[k]
                if v == old_v:
                    continue
                print(f'Overriding {k} from {old_v} to {v}')
                obj_dict[k] = v
        
        sim = Simulation(**obj_dict)

        return sim            

    def set_agents_genotype_phenotype(self):
        
        self.genotypes = np.array([
            self.genotype_populations[i,self.genotype_index]
            for i in range(self.num_agents)
        ])

        self.phenotypes = [{} for _ in range(self.num_agents)]

        for i, a in enumerate(self.agents):
            a.genotype_to_phenotype(
                self.genotypes[i],
                phenotype_dict=self.phenotypes[i]
            )

        if self.data_record is not None:
            self.data_record['genotypes'] = self.genotypes
            self.data_record['phenotypes'] = self.phenotypes            
            self.data_record['signatures'] = [
                utils.get_numpy_signature(gt) 
                for gt in self.genotypes
            ]

    def init_data_record(self):
        if self.data_record is None:
            return
        self.data_record['positions'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, 2))
        self.data_record['angles'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents))
        
        self.data_record['centroid_pos'] = self.centroid_pos        
        self.data_record['formation'] = self.formation
        
        self.data_record['signals'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_sensors))
        self.data_record['sensors'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_sensors))
        self.data_record['brain_inputs'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_neurons))
        self.data_record['brain_states'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_neurons))
        self.data_record['brain_derivatives'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_neurons))
        self.data_record['brain_outputs'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_neurons))
        self.data_record['motors'] = \
            np.zeros((self.num_trials, self.num_steps, self.num_agents, self.num_motors))

    def save_data_record_step(self, t, i, agents_pos, agents_angle):
        if self.data_record is None:
            return

        self.data_record['positions'][t][i] = agents_pos
        self.data_record['angles'][t][i] = agents_angle
        self.data_record['signals'][t][i] = np.array([a.signals_strength for a in self.agents]) # signal strenght
        self.data_record['sensors'][t][i] = np.array([a.sensors_dist for a in self.agents]) # distances
        self.data_record['brain_inputs'][t][i] = np.array([a.brain.input for a in self.agents])
        self.data_record['brain_states'][t][i] = np.array([a.brain.states for a in self.agents])
        self.data_record['brain_derivatives'][t][i] = np.array([a.brain.dy_dt for a in self.agents])
        self.data_record['brain_outputs'][t][i] = np.array([a.brain.output for a in self.agents])
        self.data_record['motors'][t][i] = np.array([a.motors for a in self.agents])
        

    def prepare_trial(self, t, random_state):                    

        # place agents in a regular polygon shape
        # their angle changes across trials of 90 degrees
        distribution_angles = np.linspace(0, 2*np.pi, self.num_agents, endpoint=False)         
        agents_pos = self.initial_distribution_radius * np.column_stack(
            [
                np.cos(distribution_angles),
                np.sin(distribution_angles)
            ]
        )
        if random_state:
            # random angles
            agents_angles = random_state.uniform(0, 2*np.pi, self.num_agents)
        else:
            agents_angles = distribution_angles - np.pi + (t * np.pi/2)

        # init agents params
        for i,a in enumerate(self.agents):
            a.init_params(agents_pos[i], agents_angles[i], init_state=self.init_ctrnn_state)

        # init AgentsGroup
        self.agents_group = AgentsGroup(self.agents)

    def check_formation(self, t, i, agents_pos):

        centr_pos = np.mean(agents_pos, axis=0)

        max_dist_centr = np.max(
            distance.cdist(
                [centr_pos], 
                agents_pos, 
                'euclidean'
            )
        )

        self.centroid_pos[t,i] = centr_pos
        self.formation[t,i] = max_dist_centr <= self.formation_radius_threshold

    def compute_travel_performance(self, centroid_positions, formation):
        # which segments are valid (agents are in formation in subsequent time steps)
        valid_segments = np.logical_and(formation[:-1], formation[1:])        

        # vectors between subsequent centroid positons
        centroid_segments = np.diff(centroid_positions, axis=0)        

        # distance between subsequent centroid positions (lengths of segments)
        centr_segm_dists = np.hypot( 
            centroid_segments[:,0], 
            centroid_segments[:,1]
        )
        
        # take only valid segments distances (those marked as True in valid_segmnets)
        # remember that: np.array([1,2,3,4])[np.array([True, False, True, False])] = [1, 3]
        valid_centr_segm_dists = centr_segm_dists[valid_segments]    

        perf = np.sum(valid_centr_segm_dists)        

        return perf, valid_segments, centroid_segments

    
    def compute_dispacement_performance(self, centroid_positions, formation):
        # which segments are valid (agents are in formation in subsequent time steps)
        valid_segments = np.logical_and(formation[:-1], formation[1:])        

        # distance between centroid and its starting position (origin) at each step
        centr_dist_from_start = np.hypot( 
            centroid_positions[:,0], 
            centroid_positions[:,1]
        )

        # how much centroid moved away from origin (positive) or back (negative) in each subsequent step
        delta_centr_dist_from_start = np.diff(centr_dist_from_start)
        
        # indexes of subsequent steps with positive delta (centroid moving away)
        positive_deltas = delta_centr_dist_from_start > 0
        valid_displacements = np.logical_and(valid_segments, positive_deltas)

        # dispacements in subsequent centroid steps (measured in distances from origin)
        centroid_displacements = np.stack(
            (
                centr_dist_from_start[:-1], 
                centr_dist_from_start[1:], 
            ),
            axis = 1
        )

        # positive and valid ranges
        valid_centroid_displacements = \
            centroid_displacements[valid_displacements]


        range_set = RangeSet(
            Range(*p) 
            for p in valid_centroid_displacements
        )

        perf = np.sum([r.length() for r in range_set])

        return perf, valid_displacements, centroid_displacements

    def compute_ratio_performance(self, centroid_positions, formation):
        perf_travel, valid_segments, centroid_segments = \
            self.compute_travel_performance(centroid_positions, formation)
        perf_displacement, valid_displacements, centroid_displacements = \
            self.compute_dispacement_performance(centroid_positions, formation)
        if perf_travel == 0:
            perf = 0
        else:
            perf = perf_displacement**2 / perf_travel
        return perf, [valid_segments, valid_displacements], [centroid_segments, centroid_displacements]
        
    def get_func_measure(self):
        if self.performance_measure == 'TRAVEL':
            return self.compute_travel_performance
        if self.performance_measure == 'DISPLACEMENT':
            return self.compute_dispacement_performance        
        return self.compute_ratio_performance # RATIO

    #################
    # MAIN FUNCTION
    #################
    def run_simulation(self, genotype_populations, genotype_index, 
                       init_ctrnn_state=0., random_state=None, data_record_dict=None):
        '''
        Main function to run simulation
        '''
        
        num_pop, pop_size, gen_size = genotype_populations.shape
        
        assert num_pop == self.num_agents, \
            f'num_pop ({num_pop}) must be equal to num_agents ({self.num_agents})'

        assert genotype_index < pop_size, \
            f'genotype_index ({genotype_index}) must be < pop_size ({pop_size})'

        assert gen_size == self.genotype_size, \
            f'invalid gen_size ({gen_size}) must be {self.genotype_size}'
            
        self.genotype_populations = genotype_populations
        self.genotype_index = genotype_index
        self.init_ctrnn_state = init_ctrnn_state
        self.data_record = data_record_dict        

        # SIMULATIONS START
        
        self.set_agents_genotype_phenotype()

        trials_performances = []

        # INITIALIZE DATA RECORD
        self.init_data_record()

        # TRIALS START
        for t in range(self.num_trials):

            # setup trial (agents positions and angles)
            self.prepare_trial(t, random_state)  

            for i in range(self.num_steps): 

                # retured pos and angles are before moving the agents
                agents_pos, agents_angle, _ = \
                    self.agents_group.full_step()                

                self.save_data_record_step(t, i, agents_pos, agents_angle)
                
                self.check_formation(t, i, agents_pos)

            func_measure = self.get_func_measure()

            trial_perf, _, _ = \
                func_measure(
                    self.centroid_pos[t],
                    self.formation[t]
                )

            trials_performances.append(trial_perf)

        # TRIALS END

        # returning min performances between all trials
        if PERF_FUNC == 'min':
            performance = np.min(trials_performances)
        else:
            performance = np.mean(trials_performances)

        if self.data_record:
            self.data_record.update({
                'genotype_index': self.genotype_index,
                'genotype_distance': utils.genotype_group_distance(self.genotypes),
                'trials_performances': trials_performances,
                'performance': performance,
            })

        return performance
    

    ##################
    # EVAL FUNCTION
    ##################
    def evaluate(self, genotype_populations, random_seed):

        assert genotype_populations.ndim == 3

        num_pop, pop_size, gen_size = genotype_populations.shape

        expected_perf_shape = (num_pop, pop_size)

        split_population = num_pop == 1

        if split_population:
            assert pop_size % self.num_agents == 0, \
                f"pop_size ({pop_size}) must be a multiple of num_agents {self.num_agents}"
            genotype_populations = np.array(
                np.split(genotype_populations[0], self.num_agents)
            )
            num_pop, pop_size, gen_size = genotype_populations.shape

        if self.num_cores == 1:
            # single core                
            group_performances = [
                self.run_simulation(genotype_populations, i)
                for i in range(pop_size)
            ]
        else:
            # run parallel job            
            group_performances = Parallel(n_jobs=self.num_cores)(
                delayed(self.run_simulation)(genotype_populations, i) \
                for i in range(pop_size)
            )

        if split_population:
            # population was split in num_agents parts
            # we need to repeat the performance of each group of agents 
            # (those sharing the same index)
            performances = np.tile([group_performances], self.num_agents) # shape: (num_agents,)            
        else:
            # we have num_agents populations
            # so we need to repeat the performances num_agetns times
            performances = np.repeat([group_performances], self.num_agents, axis=0)
        
        assert performances.shape == expected_perf_shape

        return performances


# --- END OF SIMULATION CLASS


# TEST

def test_simulation(num_agents=2, num_sensors=4, num_neurons=4, 
                    num_steps=100, seed=None):
    
    from pyevolver.evolution import Evolution    

    if seed is None:
        seed = utils.random_int()

    print("Seed: ", seed)

    sim = Simulation(
        num_agents,
        num_sensors,
        num_neurons,
        num_steps=num_steps
    )

    rs = RandomState(seed)

    genotype_populations = np.array([
        [Evolution.get_random_genotype(rs, sim.genotype_size)]
        for _ in range(sim.num_agents)
    ])

    data_record_dict = {}

    performance = sim.run_simulation(
        genotype_populations=genotype_populations, 
        genotype_index=0, 
        data_record_dict=data_record_dict
    )
    print("Performance: ", performance)
    print("Trial Performances: ", data_record_dict['trials_performances'])

    return sim, data_record_dict


if __name__ == "__main__":
    test_simulation(
        num_agents=2, 
        num_sensors=4, 
        num_neurons=4
    )
