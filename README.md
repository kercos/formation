# Agents Formation

## Publication
This is the code behind the publication:\
"Between Individual Brains and Collective Behavior: Multi-level Emergence in a Group Formation Task"

Below are the videos of the simulation reported in the paper:

![Gen 100](pub_video/3p_3a_4s_4n_zfill_noshuffle_seed_019_t1_e0100.mp4)\
![Gen 500](pub_video/3p_3a_4s_4n_zfill_noshuffle_seed_019_t1_e0500.mp4)\
![Gen 5000](pub_video/3p_3a_4s_4n_zfill_noshuffle_seed_019_t1_e5000.mp4)

